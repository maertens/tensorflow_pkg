# TensorFlow_PKG

Contains custom tensorflow builds packages:

- Tensorflow v2.6.0, Cuda 10.1, Compute capability 3.0 (v2.6.0_cuda10.1_cc3.0)

## Download

`git clone https://gitlab.stce.rwth-aachen.de/maertens/tensorflow_pkg.git`

## Installation (Root)

### Python3
`pip3 install tensorflow_pkg/v2.6.0_cuda10.1_cc3.0/v2.6.0_cuda10.1_cc3.0/tensorflow-2.6.0-cp36-cp36m-linux_x86_64.whl`

### C API
`sudo tar -C /usr/local -xzf tensorflow_pkg//v2.6.0_cuda10.1_cc3.0/libtensorflow.tar.gz`

## Installation (User)

### Python3
`pip3 install --user tensorflow_pkg/v2.6.0_cuda10.1_cc3.0/tensorflow-2.6.0-cp36-cp36m-linux_x86_64.whl`

### C API
`tar -C <YOUR_INSTALL_DIR> -xzf tensorflow_pkg//v2.6.0_cuda10.1_cc3.0/libtensorflow.tar.gz`
